package me.baghino.spark.streaming.twitter.example

import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.streaming.twitter.TwitterUtils
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.rdd._
import org.apache.spark.sql._
import org.apache.spark.sql.types._
import twitter4j.Status
import main.scala.LMPipeline

object TwitterSentimentScore extends App {

  val pipeline = new LMPipeline
  //pipeline.xxx()

  // You can find all functions used to process the stream in the
  // Utils.scala source file, whose contents we import here
  import Utils._

  // First, let's configure Spark
  // We have to at least set an application name and master
  // If no master is given as part of the configuration we
  // will set it to be a local deployment running an
  // executor per thread
  val sparkConfiguration = new SparkConf().
    setAppName("spark-twitter-stream-example").
    setMaster(sys.env.get("spark.master").getOrElse("local[*]"))

  // Let's create the Spark Context using the configuration we just created
  val sparkContext = new SparkContext(sparkConfiguration)
  val sqlContext = new SQLContext(sparkContext)

  // Now let's wrap the context in a streaming one, passing along the window size
  val streamingContext = new StreamingContext(sparkContext, Seconds(1))

  // Creating a stream from Twitter (see the README to learn how to
  // provide a configuration to make this work - you'll basically
  // need a set of Twitter API keys)
  val tweets: DStream[Status] =
    TwitterUtils.createStream(streamingContext, None)

  // To compute the sentiment of a tweet we'll use different set of words used to
  // filter and score each word of a sentence. Since these lists are pretty small
  // it can be worthwhile to broadcast those across the cluster so that every
  // executor can access them locally
  val uselessWords = sparkContext.broadcast(load("/stop-words.dat"))
  val positiveWords = sparkContext.broadcast(load("/pos-words.dat"))
  val negativeWords = sparkContext.broadcast(load("/neg-words.dat"))

  // tweets have these methods: http://twitter4j.org/oldjavadocs/2.1.0/twitter4j/StatusJSONImpl.html
  tweets.
    count().
    print

  tweets.foreachRDD(rdd => {

    val seq: RDD[Row] = rdd.map(record => {
      Row(
        record.getCreatedAt(),
        record.getId(),
        record.getText(),
        record.getSource(),
        record.isTruncated(),
        record.getInReplyToStatusId(),
        record.getInReplyToUserId(),
        record.getInReplyToScreenName(),
        record.getGeoLocation(),
        record.isFavorited(),
        record.getUser(),
        record.isRetweet(),
        record.getRetweetedStatus(),
				record.retweetCount
      )
    })
    //.fold(Seq(0))(_ ++ _)
    //println("****************** " +  Row.fromSeq(seq))

		val schema = StructType(
      StructField("createdAt", DateType, false) ::
      StructField("id", LongType, false) ::
      StructField("text", StringType, false) ::
      StructField("source", StringType, false) ::
      StructField("isTruncated", BooleanType, false) ::
      StructField("inReplyToStatusId", LongType, false) ::
      StructField("inReplyToUserId", LongType, false) ::
      StructField("inReplyToScreenName", StringType, false) ::
      StructField("geoLocation", StringType, false) ::
      StructField("isFavorited", BooleanType, false) ::
      StructField("user", StringType, false) ::
      StructField("isRetweet", BooleanType, false) ::
      StructField("retweetCount", IntType, false) ::
      StructField("retweetedStatus", StringType, false) :: Nil
    )

    pipeline.train(sqlContext.createDataFrame(seq, schema))
  })

  // Now that the streaming is defined, start it
  streamingContext.start()

  // Let's await the stream to end - forever
  streamingContext.awaitTermination()

}
