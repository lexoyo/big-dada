package main.scala

// code from here : https://spark.apache.org/docs/2.1.0/ml-pipeline.html

import org.apache.spark.ml.classification.LogisticRegression
import org.apache.spark.ml.classification.LogisticRegressionModel
import org.apache.spark.ml.linalg.{Vector, Vectors}
import org.apache.spark.ml.param.ParamMap
import org.apache.spark.sql.Row
import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf

class LMPipeline {
  def xxx() {
    val sparkSession = SparkSession.builder()
    .master("local")
    .appName("Word Count")
    .getOrCreate()
    
    // Prepare training data from a list of (label, features) tuples.
    val model = train(sparkSession.createDataFrame(Seq(
      (1.0, Vectors.dense(0.0, 1.1, 0.1)),
        (0.0, Vectors.dense(2.0, 1.0, -1.0)),
      (0.0, Vectors.dense(2.0, 1.3, 1.0)),
        (1.0, Vectors.dense(0.0, 1.2, -0.5))
    )).toDF("label", "features"))
    // Prepare test data.
    test(model, sparkSession.createDataFrame(Seq(
      (1.0, Vectors.dense(-1.0, 1.5, 1.3)),
      (0.0, Vectors.dense(3.0, 2.0, -0.1)),
      (1.0, Vectors.dense(0.0, 2.2, -1.5))
    )).toDF("label", "features"))    
  }
  def train(df: DataFrame): LogisticRegressionModel = {

    // Create a LogisticRegression instance. This instance is an Estimator.
    val lr = new LogisticRegression()
    .setMaxIter(10)
    .setRegParam(0.01)
    
    // Print out the parameters, documentation, and any default values.
    println("**********************\nLogisticRegression parameters:\n" + lr.explainParams() + "\n")
    
    // Learn a LogisticRegression model. This uses the parameters stored in lr.
    val model = lr.fit(df)

    // Since model is a Model (i.e., a Transformer produced by an Estimator),
    // we can view the parameters it used during fit().
    // This prints the parameter (name: value) pairs, where names are unique IDs for this
    // LogisticRegression instance.
    println("**********************\nModel was fit using parameters: " + model.parent.extractParamMap)

    return model
  }
  def test(model: LogisticRegressionModel, df: DataFrame) {
   
    println("**********************")
    // Make predictions on test data using the Transformer.transform() method.
    // LogisticRegression.transform will only use the 'features' column.
    model.transform(df)
    .select("features", "label", "probability", "prediction")
    .collect()
    .foreach { case Row(features: Vector, label: Double, prob: Vector, prediction: Double) =>
      println(s"($features, $label) -> prob=$prob, prediction=$prediction")
    }
  }
}
